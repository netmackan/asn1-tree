/*
 * Copyright 2018 Markus Kilås.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.kilas.markus.asn1tree.browser;

import java.util.Enumeration;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import org.bouncycastle.asn1.ASN1Boolean;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Enumerated;
import org.bouncycastle.asn1.ASN1GeneralizedTime;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1UTCTime;
import org.bouncycastle.asn1.BERApplicationSpecific;
import org.bouncycastle.asn1.BEROctetString;
import org.bouncycastle.asn1.BERSequence;
import org.bouncycastle.asn1.BERSet;
import org.bouncycastle.asn1.DERApplicationSpecific;
import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.DERExternal;
import org.bouncycastle.asn1.DERGraphicString;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DERPrintableString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERT61String;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.DERVideotexString;
import org.bouncycastle.asn1.DERVisibleString;

/**
 * ASN.1 parser building JTree nodes.
 * Based on ASN1Dump from Bouncy Castle.
 *
 * @author Markus Kilås
 */
public class Asn1Tree {

    public static void parseNodes(DefaultMutableTreeNode node, ASN1Primitive obj) {
        if (obj instanceof ASN1Sequence) {
            Enumeration objects = ((ASN1Sequence) obj).getObjects();
            DefaultMutableTreeNode node2;
            
            if (obj instanceof BERSequence) {
               node2 = new DefaultMutableTreeNode("BER Sequence", true);
            } else if (obj instanceof DERSequence) {
               node2 = new DefaultMutableTreeNode("DER Sequence", true);
            } else {
               node2 = new DefaultMutableTreeNode("Sequence", true);
            }
            node.add(node2);
            
            parseChilds(node2, objects);
        } else if (obj instanceof ASN1TaggedObject) {
            final String tagged;
            if (obj instanceof DERTaggedObject) {
                tagged = "BER Tagged [";
            } else {
                tagged = "Tagged [";
            }
            
            ASN1TaggedObject o = (ASN1TaggedObject)obj;
            int tagNo = o.getTagNo();
            
            DefaultMutableTreeNode node2 = new DefaultMutableTreeNode(tagged + String.valueOf(tagNo) + "]" + (o.isExplicit() ? "" : " IMPLICIT"));
            node.add(node2);
            
            if (o.isEmpty()) {
                node2.add(new DefaultMutableTreeNode("EMPTY", false));
            } else {
                parseNodes(node2, o.getObject());
            }
        } else if (obj instanceof ASN1Set) {
            final DefaultMutableTreeNode node2;
            if (obj instanceof BERSet) {
                node2 = new DefaultMutableTreeNode("BER Set", true);
            } else {
                node2 = new DefaultMutableTreeNode("DER Set", true);
            }
            node.add(node2);
            
            final Enumeration objects = ((ASN1Set) obj).getObjects();
            parseChilds(node2, objects);
        } else if (obj instanceof ASN1OctetString) {
            ASN1OctetString oct = (ASN1OctetString)obj;

            if (obj instanceof BEROctetString) {
                node.add(new DefaultMutableTreeNode("BER Constructed Octet String" + "[" + oct.getOctets().length + "] ", false));
            } else {
                node.add(new DefaultMutableTreeNode("DER Octet String" + "[" + oct.getOctets().length + "] ", false));
            }
        } else if (obj instanceof ASN1ObjectIdentifier) {
            node.add(new DefaultMutableTreeNode("ObjectIdentifier(" + ((ASN1ObjectIdentifier)obj).getId() + ")", false));
        } else if (obj instanceof ASN1Boolean) {
            node.add(new DefaultMutableTreeNode("TODO: ASN1Dump.java:243", false));
        } else if (obj instanceof ASN1Integer) {
            node.add(new DefaultMutableTreeNode("Integer(" + ((ASN1Integer) obj).getValue() + ")", false));
        } else if (obj instanceof DERBitString) {
            node.add(new DefaultMutableTreeNode("TODO: ASN1Dump.java:251", false));
        } else if (obj instanceof DERIA5String) {
            node.add(new DefaultMutableTreeNode("TODO: ASN1Dump.java:264", false));
        } else if (obj instanceof DERUTF8String) {
            node.add(new DefaultMutableTreeNode("UTF8String(" + ((DERUTF8String) obj).getString() + ")", false));
        } else if (obj instanceof DERPrintableString) {
            node.add(new DefaultMutableTreeNode("PrintableString(" + ((DERPrintableString)obj).getString() + ")", false));
        } else if (obj instanceof DERVisibleString) {
            node.add(new DefaultMutableTreeNode("TODO: ASN1Dump.java:276", false));
        } else if (obj instanceof DERBMPString) {
            node.add(new DefaultMutableTreeNode("TODO: ASN1Dump.java:280", false));
        } else if (obj instanceof DERT61String) {
            node.add(new DefaultMutableTreeNode("TODO: ASN1Dump.java:284", false));
        } else if (obj instanceof DERGraphicString) {
            node.add(new DefaultMutableTreeNode("TODO DERGraphicString", false));
        } else if (obj instanceof DERVideotexString) {
            node.add(new DefaultMutableTreeNode("TODO DERVideotexString", false));
        } else if (obj instanceof ASN1UTCTime) {
            node.add(new DefaultMutableTreeNode("UTCTime(" + ((ASN1UTCTime)obj).getTime() + ") ", false));
        } else if (obj instanceof ASN1GeneralizedTime) {
            node.add(new DefaultMutableTreeNode("TODO ASN1GeneralizedTime", false));
        } else if (obj instanceof BERApplicationSpecific) {
            node.add(new DefaultMutableTreeNode("TODO BERApplicationSpecific", false));
        } else if (obj instanceof DERApplicationSpecific) {
            node.add(new DefaultMutableTreeNode("TODO DERApplicationSpecific", false));
        } else if (obj instanceof ASN1Enumerated) {
            node.add(new DefaultMutableTreeNode("TODO ASN1Enumerated", false));
        } else if (obj instanceof DERExternal) {
            node.add(new DefaultMutableTreeNode("TODO DERExternal", false));
        } else {
            node.add(new DefaultMutableTreeNode("else: " + (obj == null ? "null" : obj.getClass()), false));
        }
    }

    private static void parseChilds(DefaultMutableTreeNode node2, Enumeration objects) {
        while (objects.hasMoreElements()) {
            Object o = objects.nextElement();

            if (o == null || o.equals(DERNull.INSTANCE)) {
                node2.add(new DefaultMutableTreeNode("NULL", false));
            } else if (o instanceof ASN1Primitive) {
                parseNodes(node2, (ASN1Primitive) o);
            } else {
                parseNodes(node2, ((ASN1Encodable) o).toASN1Primitive());
            }
        }
    }
}
