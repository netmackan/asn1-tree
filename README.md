# asn1-tree

Parse a binary and display ASN.1 in tree form.


## Building

```
cd asn1-tree-browser
mvn install
```

## Running

```
java -jar target/asn1-tree-browser.jar
```

A window is openned displaying the tree.


## Developing

Open in your preferred IDE, i.e. NetBeans IDE.
